package com.company;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

public class Scheduler {

    // Kelsey Ferguson - This linked list represents the ready queue
    static LinkedList<Task> readyQueue = new LinkedList<Task>();
    int algo;

    // Kelsey Ferguson - The number of tasks and number of arrived tasks are kept track of to know when to start the
    // dispatchers
    int numberOfTasks;
    static int arrivedTasks;

    static Semaphore startDispatchers = new Semaphore(0); // Kelsey Ferguson - For signalling to fork dispatchers

    // Kelsey Ferguson - This mutex restricts access to the ready queue to one thread at a time
    static Semaphore mutex = new Semaphore(1);

    Task currentRunningTask;

    //Kelsey Ferguson - This semaphore is to ensure that when comparing a new task to a currently running task,
    //the currently running task doesn't leave the processor and lead to an error due to currentRunningTask
    //being null
    static Semaphore checkCurrentTask = new Semaphore(1);

    public Scheduler(int algo, int numberOfTasks) {
        this.algo = algo;
        this.numberOfTasks = numberOfTasks;
    }


    /**
     * Kelsey Ferguson
     * The tasks call this method when entering the ready queue. If the algorithm is either PSJF OR NPSJF, the
     * ready queue is sorted from tasks with the shortest remaining burst times to tasks with the longest remaining burst
     * times. This function also tracks the arrivedTasks to determine when all tasks are in the ready queue and
     * the dispatchers can be forked.
     */
    public void addToQueue(Task task) {

        try {
            mutex.acquire();

            readyQueue.add(task);

            if (algo == 3 || algo == 4)
                readyQueue.sort(new sortList());

            if (arrivedTasks < numberOfTasks) {
                arrivedTasks++;
                if (arrivedTasks == numberOfTasks)
                    startDispatchers.release();
            }

            // Kelsey Ferguson - If the taskID is equal to or greater than numberOfTasks, it must be a new task for PSJF
            if (algo == 4 && task.taskID >= numberOfTasks) {
                checkCurrentTask.acquire();
                if (currentRunningTask != null) {
                    currentRunningTask.newTaskArriving.acquire();
                    printQueue();
                    if (task.maxBurstTime < currentRunningTask.maxBurstTime - currentRunningTask.currentBurstTime)
                        currentRunningTask.keepGoing = false;
                    currentRunningTask.newTaskArriving.release();
                }
                else
                    printQueue();

                checkCurrentTask.release();
            }

            mutex.release();

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Kelsey Ferguson
     * This function is for outputting the contents of the ready queue
     */
    public void printQueue() {
        System.out.println("------------- Ready Queue -------------");
        for (Task task : readyQueue)
            System.out.printf("Task %s, Max Burst: %s, Current Burst: %s%n", task.taskID, task.maxBurstTime,
                    task.currentBurstTime);
        System.out.println("----------------------------------------");
    }

    /**
     * Kelsey Ferguson
     * It is the job of the scheduler to find the task to run next based on the chosen scheduling algorithm. The
     * dispatcher calls this function and the scheduler chooses a task and returns that task to the dispatcher.
     * If the ready queue is empty, null is returned and the dispatcher handles that scenario.
     */
    public Task findTask() {

        Task task = null;

        try {

            mutex.acquire();

            task = readyQueue.pollFirst();

            if (algo == 4) {
                checkCurrentTask.acquire();
                currentRunningTask = task;
                checkCurrentTask.release();
            }

            mutex.release();



        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return task;
    }

    /**
     * Kelsey Ferguson
     * This function is called in Main and signals when to fork the dispatchers.
     */
    public void startDispatchers() {

        try {
            startDispatchers.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException();
        }
    }


    /**
     * Kelsey Ferguson
     * This class implements comparator and handles sorting the ready queue if NPSJF or PSJF are being used.
     */
    static class sortList implements Comparator<Task> {

        @Override
        public int compare(Task o1, Task o2) {
            return (o1.maxBurstTime - o1.currentBurstTime) > (o2.maxBurstTime - o2.currentBurstTime) ? 1 : -1;
        }
    }
}
