package com.company;

import java.util.concurrent.Semaphore;

public class Dispatcher implements Runnable {

    // Kelsey Ferguson - These four variables are for a barrier that ensures all dispatchers are ready before dispatching
    int numberOfDispatchers;
    static int arrivedDispatchers;
    static Semaphore mutex = new Semaphore(1);
    static Semaphore dispatchersBarrier = new Semaphore(0);

    int dispatcherID; // Kelsey Ferguson - The CPU and dispatcher share an ID

    Scheduler scheduler; // Kelsey Ferguson - The dispatcher calls the scheduler to pick a task

    // Kelsey Ferguson - The dispatcher waits until its respective CPU is not busy
    Semaphore dispatch = new Semaphore(1);

    // Kelsey Ferguson - If the number of finished tasks equals the number of tasks, the dispatchers can stop.
    static int finishedTasks;

    int numberOfTasks;

    int algo;
    public Dispatcher(int dispatcherID, int numberOfTasks, Scheduler scheduler, int numberOfDispatchers, int algo) {
        this.dispatcherID = dispatcherID;
        this.numberOfTasks = numberOfTasks;
        this.scheduler = scheduler;
        this.numberOfDispatchers = numberOfDispatchers;
        this.algo = algo;

    }

    /**
     * Kelsey Ferguson
     * After forking the dispatchers, the dispatchers wait at a barrier for all dispatchers to be ready before they
     * start dispatching. Once all dispatchers are ready, the dispatchers start dispatching until finishedTasks is
     * equal to numberOfTasks. Before calling the scheduler, a dispatcher must wait until its respective CPU isn't
     * already busy. Once the CPU signals the dispatch semaphore, the scheduler is called and, if the ready queue isn't
     * empty, it returns a task to run on the processor. If no task is returned, the dispatcher signals the dispatch
     * semaphore instead of the CPU and tries to retrieve a task again.
     */
    @Override
    public void run() {

        try {
            mutex.acquire();

            System.out.printf("Dispatcher %s is using CPU %s %n", dispatcherID, dispatcherID);

            arrivedDispatchers++;

            if (arrivedDispatchers == numberOfDispatchers) {
                System.out.printf("-----------------------------%n");
                for (int i = 0; i < numberOfDispatchers-1; i++)
                    dispatchersBarrier.release();
                mutex.release();
            }
            else {
                mutex.release();
                dispatchersBarrier.acquire();
            }

            // Broadcast algorithm
            System.out.printf("Dispatcher %d   | Running %s algorithm\n", dispatcherID, algo == 1 ? "FCFS" : algo == 2 ? "RR" : algo == 3 ? "N-SJF" : "P-SJF");

            while (finishedTasks < numberOfTasks) {

                dispatch.acquire();

                Task task = scheduler.findTask();

                if (task != null) {
                    task.cpuInUse = dispatcherID;
                    //System.out.printf("%n-------------------------------------------%n");
                    System.out.printf("Dispatcher %s   | Running task %s on CPU %s %n", dispatcherID, task.taskID,
                            dispatcherID);
                    CPU(task);
                } else
                    dispatch.release();

            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Kelsey Ferguson
     * The CPU receives a task from the dispatcher and updates its goalBurstTime to determine how long the task should
     * ideally run in this one cycle. It signals that task to start running and waits until it's done. Once the task is
     * done running, if its current burst time is equal to its max burst time, the task is finished and finishedTasks
     * is updated. The CPU then signals the dispatcher that it's ready for another task.
     */
    public void CPU(Task task) {

        try {
            /* Collin Varisco
             * FCFS - For each task, goalBurstTime will be set once to maxBurstTime.
             * RR - goalBurstTime will be incremented by the time quantum if (currentBurstTime + burstTime) < maxBurstTime
             *      Otherwise, it will be set to maxBurstTime to finish the executing the task.
            */
            task.goalBurstTime = Math.min(task.currentBurstTime + task.burstTime, task.maxBurstTime);

            task.run.release();

            task.cpuSignal.acquire();

            if (task.currentBurstTime == task.maxBurstTime)
                finishedTasks++;

            dispatch.release();

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
