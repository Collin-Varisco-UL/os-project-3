package com.company;

import java.util.Random;

public class Main {
    /** Collin Varisco
     * This is a method that outputs the algorithm that will be used along with any possible options
     * the user chose for the algorithm like time quantum or a different number of cores (4 by default).
     * @param algo - The algorithm that will be used.
     * @param quantum - The time quantum that will be used for the Round Robin algorithm.
     * @param cores - The number of cores that will be used during simulation.
     */
    public static void runAlgorithm(int algo, int quantum, int cores){
        if(algo == 1)
            System.out.printf("FCFS (Cores: %s)%n", cores);
        else if(algo == 2)
            System.out.printf("RR (Cores: %s, Time Quantum: %s)%n", cores, quantum);
        else if(algo == 3)
            System.out.printf("N-SJF (Cores: %s)%n", cores);
        else if(algo == 4)
            System.out.printf("P-SJF (Cores: %s)%n", cores);

        /**
         * Kelsey Ferguson
         * The task and dispatcher threads are being created. Additionally, there is a scheduler class containing the
         * ready queue that the dispatcher and task class receive. This scheduler class contains the functions for
         * tasks and the dispatchers to interact with the ready queue.
         */

        Random randomGen = new Random();

        int numberOfTasks = randomGen.nextInt(1,26);

        Scheduler scheduler = new Scheduler(algo, numberOfTasks);

        Thread[] tasks = new Thread[numberOfTasks];
        Thread[] dispatchers = new Thread[cores];

        for (int i = 0; i < numberOfTasks; i++) {
            System.out.printf("Main thread     | Creating Task Thread %s%n", i);
            tasks[i] = new Thread(new Task(i, scheduler, randomGen.nextInt(1,51), quantum, algo));
        }

        /**
         * Kelsey Ferguson
         * For PSJF, the newTasks array will contain 3 new tasks. Before creating the dispatcher instance for PSJF,
         * the numberOfTasks variable needs to have 3 added on so the dispatcher is aware of how many tasks need to
         * be completed unlike the scheduler instance which only needs to know the initial amount in the queue to know
         * when to fork the dispatchers.
         */
        Thread[] newTasks = new Thread[3];
        if (algo == 4) {
            numberOfTasks = numberOfTasks + 3;
            newTasks = new Thread[3];
        }

        for (int i = 0; i < cores; i++)
            dispatchers[i] = new Thread(new Dispatcher(i, numberOfTasks, scheduler, cores, algo));

        /** Kelsey Ferguson
         * Starting the tasks
         */

        for (Thread task : tasks)
            task.start();

        /**
         * Kelsey Ferguson
         * Waiting for all the tasks to arrive. This function acquires a semaphore.
         */
        scheduler.startDispatchers();

        System.out.printf("%nMain thread: All tasks have arrived. %n");

        scheduler.printQueue();

        /** The dispatches are being forked */
        System.out.printf("%n%n-----Forking dispatchers-----%n");

        for (Thread dispatcher : dispatchers) {
            dispatcher.start();
        }

        /** Kelsey Ferguson
         * The new tasks have a burst time between 1 and 10. The main thread sleeps for 5 milliseconds before
         * forking each new task.
         */
        if (algo == 4) {
            for (int i = 0; i < 3; i++) {
                newTasks[i] = new Thread(new Task((numberOfTasks - 3) + i, scheduler,
                        randomGen.nextInt(1,10), 0, algo));
            }
            for (int i = 0; i < 3; i++) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                newTasks[i].start();
            }
        }
    }

    /** Collin Varisco
     * @param args - The arguments passed from the command line.
     *
     *          Valid Arguments to run any algorithm on 4 cores:
     *             -S <algorithm> <time quantum (if applicable)>
     *
     *          Arguments for specifying the number of cores:
     *             -S <algorithm> <Time Quantum (if applicable)> -C <number of cores>
     *
     *          Algorithms:
     *             1 - FCFS
     *             2 - RR
     *             3 - NPSJF
     *             4 - PSJF - Does not run with more than 1 core.
     */
    static void handleCommandLineArguments (String[] args) {
        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("-s")) {
                try {
                    int algorithm = Integer.parseInt(args[1]);
                    if (algorithm == 1 || algorithm == 3 || algorithm == 4) {
                        runAlgorithm(algorithm, 0, 1);
                    } else {
                        if(algorithm == 2)
                            System.out.println("You must specify the time quantum for the Round Robin algorithm.");
                        else
                            System.out.println("Invalid algorithm.");
                    }
                } catch (Exception e) { System.out.println("Invalid arguments: " + e); }
            } else {
                System.out.println("Invalid arguments.");
            }
        } else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("-s") && args[1].equals("2")) {
                try {
                    int algorithm = Integer.parseInt(args[1]);
                    int timeQuantum = Integer.parseInt(args[2]);
                    if (timeQuantum >= 2 && timeQuantum <= 10)
                        runAlgorithm(algorithm, timeQuantum, 1);
                    else
                        System.out.println("Time quantum must be between 2 and 10.");
                } catch (Exception e) { System.out.println("Invalid arguments: " + e); }
            } else {
                System.out.println("Invalid arguments.");
            }
        } else if (args.length == 4) {
            if (args[0].equalsIgnoreCase("-s") && args[2].equalsIgnoreCase("-c")) {
                try {
                    int algorithm = Integer.parseInt(args[1]);
                    int cores = Integer.parseInt(args[3]);
                    if (cores >= 1 && cores <= 4) {
                        if (algorithm == 4 && cores > 1)
                            System.out.println("PSJF can only be run on one core.");
                        else
                            runAlgorithm(algorithm, 0, cores);
                    } else
                        System.out.println("Invalid number of cores");
                } catch (Exception e) { System.out.println("Invalid arguments: " + e); }
            } else if (args[0].equalsIgnoreCase("-c") && args[2].equalsIgnoreCase("-s")) {
                try {
                    int algorithm = Integer.parseInt(args[3]);
                    int cores = Integer.parseInt(args[1]);
                    if (cores >= 1 && cores <= 4) {
                        if (algorithm == 4 && cores > 1)
                            System.out.println("PSJF can only be run on one core.");
                        else
                            runAlgorithm(algorithm, 0, cores);
                    } else
                        System.out.println("Invalid number of cores");
                } catch (Exception e) { System.out.println("Invalid arguments: " + e); }
            } else {
                System.out.println("Invalid arguments.");
            }
        } else if (args.length == 5) {
            if (args[0].equalsIgnoreCase("-s") && args[3].equalsIgnoreCase("-c")) {
                try {
                    int algorithm = Integer.parseInt(args[1]);
                    int quantum = Integer.parseInt(args[2]);
                    int cores = Integer.parseInt(args[4]);
                    if (cores > 0 && cores <= 4 && quantum > 2 && quantum <= 10)
                        runAlgorithm(algorithm, quantum, cores);
                    else {
                        if(cores > 4 || cores < 1)
                            System.out.println("Invalid number of cores");
                        else if(quantum < 2 || quantum > 10)
                            System.out.println("Invalid time quantum");
                    }
                } catch (Exception e) { System.out.println("Invalid arguments: " + e); }
            } else if (args[0].equalsIgnoreCase("-c") && args[2].equalsIgnoreCase("-s")) {
                try {
                    int algorithm = Integer.parseInt(args[3]);
                    int quantum = Integer.parseInt(args[4]);
                    int cores = Integer.parseInt(args[1]);
                    if (cores > 0 && cores <= 4 && quantum > 2 && quantum <= 10)
                        runAlgorithm(algorithm, quantum, cores);
                    else {
                        if(cores > 4 || cores < 1)
                            System.out.println("Invalid number of cores");
                        else if(quantum < 2 || quantum > 10)
                            System.out.println("Invalid time quantum");
                    }
                } catch (Exception e) { System.out.println("Invalid arguments: " + e); }
            } else {
                System.out.println("Invalid arguments.");
            }
        } else {
            System.out.println("Invalid arguments.");
        }
    }

    public static void main(String[] args){
        handleCommandLineArguments(args);
    }
}
