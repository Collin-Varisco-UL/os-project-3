package com.company;

import java.util.concurrent.Semaphore;

public class Task implements Runnable{

    int maxBurstTime; // Kelsey Ferguson - This is the total burst time of the task

    // Kelsey Ferguson - This is the total allowed burst time for one cycle on the processor. Same as maxBurstTime
    // except when using RR.
    int burstTime;

    // Kelsey Ferguson - This is currentBurstTime + burstTime unless that value is larger than maxBurstTime. In that
    // case, it's maxBurstTime.
    int goalBurstTime;
    int currentBurstTime; // Kelsey Ferguson - This is the current number of bursts

    int cpuInUse; // Kelsey Ferguson - This is set to the CPU the task is currently using.

    int taskID;

    //// Kelsey Ferguson - The scheduler class contains the ready queue and function for interacting with it.
    Scheduler scheduler;

    // Kelsey Ferguson - This semaphore ensures the tasks wait until they're put on a processor before they run.
    Semaphore run = new Semaphore(0);

    // Kelsey Ferguson - The tasks must signal the CPU they're using when done running.
    Semaphore cpuSignal = new Semaphore(0);
    int algo;

    boolean keepGoing = true; // Kelsey Ferguson - This boolean is for tasks being removed due to using PSJF

    // Kelsey Ferguson - This semaphore is for letting the queue be output without interruption when a new task arrives
    Semaphore newTaskArriving = new Semaphore(1);
    public Task(int taskID, Scheduler scheduler, int maxBurstTime, int timeQuantum, int algo) {
        this.taskID = taskID;
        this.scheduler = scheduler;
        this.maxBurstTime = maxBurstTime;
        this.algo = algo;

        if (algo == 2)
            this.burstTime = timeQuantum;
        else
            this.burstTime = maxBurstTime;
    }


    /**
     * Kelsey Ferguson
     * While currentBurstTime is less than maxBurstTime, a given task is added to the queue and waits until it's put on
     * a CPU to run. Once the CPU signals the run semaphore, the task updates its currentBurstTime in a loop until
     * either currentBurstTime is equal to goalBurstTime or it's interrupted. Once it's done running, it signals the
     * CPU that its done.
     */
    @Override
    public void run() {
        try {
            while (currentBurstTime < maxBurstTime) {

                scheduler.addToQueue(this);

                run.acquire();

                int initialBurstTime = currentBurstTime;

                while (currentBurstTime < goalBurstTime) {

                    if (algo == 4) {
                        newTaskArriving.acquire();
                        newTaskArriving.release();

                        if (!keepGoing) {
                            break;
                        }
                    }

                    if (currentBurstTime == initialBurstTime)
                        System.out.printf("\nTask %s -- CPU: %s MB: %s CB: %s BT: %s BG: %s%n",
                                taskID, cpuInUse, maxBurstTime, currentBurstTime, burstTime, goalBurstTime);

                    currentBurstTime++;
                    System.out.printf("Thread %s       | Burst %s on CPU %s%n", taskID, currentBurstTime, cpuInUse);

                }

                /**
                 * Kelsey Ferguson
                 * The output here varies depending on why the task is leaving the processor. If it was removed
                 * because of a task with a shorter burst time arriving, it must set keepGoing back to true so that
                 * it can run when put back on the processor. If the algorithm isn't PSJF, keepGoing should always
                 * be true.
                 */
                if (!keepGoing) {
                    System.out.printf("%n--------------------------------------------%n");
                    System.out.printf("Task %s was removed from the processor. %n", taskID);
                    keepGoing = true;
                }

                //System.out.printf("--------------------------------------------%n");

                /**
                 * Kelsey Ferguson
                 * The currently running task is set to null if using PSJF. Otherwise, this variable is not used.
                 */

                if (algo == 4) {
                    scheduler.checkCurrentTask.acquire();
                    scheduler.currentRunningTask = null;
                    scheduler.checkCurrentTask.release();
                }

                cpuSignal.release();
            }

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}
